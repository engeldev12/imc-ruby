module IMC

	class Persona
		attr_accessor :peso, :altura

		def initialize(peso: 0.0, altura:0.0)
			@peso = peso
			@altura = altura
		end

	end

	class Calculadora
		DECIMALES = 2
		IMC_NORMAL_MINIMO = 18.5
		IMC_NORMAL_MAXIMO = 24.99

		def self.imc persona
			(persona.peso / persona.altura ** 2).round DECIMALES
		end

		def self.esta_baja_de_peso? persona
			imc(persona) < IMC_NORMAL_MINIMO
		end

		def self.tiene_un_peso_normal? persona
			(IMC_NORMAL_MINIMO .. IMC_NORMAL_MAXIMO).include? imc(persona)
		end

		def self.tiene_sobre_peso? persona
			imc(persona) > IMC_NORMAL_MAXIMO	
		end

	end

end	