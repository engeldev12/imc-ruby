require "rake/testtask"


namespace :test do
	
	desc 'Ejecutar pruebas unitarias'

	Rake::TestTask.new(:unit) do |t|
		t.libs << 'tests'
		t.verbose = false
		t.test_files = Dir['tests/test_*.rb']
	end

end