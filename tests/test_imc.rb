require "test_helper"


class CalculadoraIMCTest < Minitest::Test

	def test_debe_estar_baja_de_peso
		
		@persona_con_peso_bajo = IMC::Persona.new peso: 40, altura: 1.60

		assert IMC::Calculadora.esta_baja_de_peso? @persona_con_peso_bajo
		
	end

	def test_debe_tener_un_peso_normal
		
		@persona_con_peso_normal = IMC::Persona.new peso: 60, altura: 1.72

		assert IMC::Calculadora.tiene_un_peso_normal? @persona_con_peso_normal

	end

	def test_debe_tener_sobre_peso

		@persona_con_sobre_peso = IMC::Persona.new peso: 80, altura: 1.60
	
		assert IMC::Calculadora.tiene_sobre_peso? @persona_con_sobre_peso

	end

	def test_debe_tener_un_metodo_para_obtener_el_imc

		assert_respond_to IMC::Calculadora, :imc
	
	end

	def test_con_el_peso_y_la_altura_obtendre_el_imc

		persona = IMC::Persona.new peso: 70, altura: 1.80

		assert_equal 21.6, IMC::Calculadora.imc(persona)
	
	end

end