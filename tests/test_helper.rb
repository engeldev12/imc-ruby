require "minitest/autorun"
require "minitest/spec"

require "simplecov"

$LOAD_PATH.unshift File.expand_path(File.join(File.dirname(__FILE__), '..', 'lib'))

SimpleCov.start do 
	add_filter '/tests/'
end

require "imc"