require "test_helper"


describe IMC::Calculadora do

	describe "cuando me dan el peso y la altura" do

		subject { IMC::Calculadora }

		it "podre obtener el imc" do
			
			@persona = IMC::Persona.new peso: 70, altura: 1.80

			_(subject.imc(@persona)).must_equal 21.6

		end

		it "podré saber si una persona tiene sobre peso" do

			@persona = IMC::Persona.new peso: 80, altura: 1.60

			_(subject.tiene_sobre_peso?(@persona)).must_equal true

		end

		it "podré saber si una persona tiene un peso normal" do

			@persona = IMC::Persona.new peso: 70, altura: 1.70

			_(subject.tiene_un_peso_normal?(@persona)).must_equal true

		end

		it "podré saber si esta baja de peso" do

			@persona = IMC::Persona.new peso: 40, altura: 1.70

			_(subject.esta_baja_de_peso?(@persona)).must_equal true

		end

		it "podre calcular el imc" do

			_(subject).must_respond_to :imc
			
		end	

	end	
	
end
