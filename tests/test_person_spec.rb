require "test_helper"


describe IMC::Persona do

	describe 'cuando creo una persona sin peso y altura' do
		
		it 'ambos datos valdrán 0.0' do

			@persona = IMC::Persona.new

			_(@persona.altura).must_equal 0.0
			_(@persona.peso).must_equal 0.0

		end
	
	end

	describe 'cuando cree una persona con su peso y altura' do
		
		it 'podré obtenerlo' do

			@persona = IMC::Persona.new peso: 70, altura: 1.70
			
			_(@persona.altura).must_equal 1.70
			_(@persona.peso).must_equal 70

		end
			
	end	

end